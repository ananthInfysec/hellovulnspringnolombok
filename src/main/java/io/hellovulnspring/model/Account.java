package io.hellovulnspring.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "account")
public class Account {

	public long getId() {
		return id;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	public void setType(String type) {
		this.type = type;
	}

	public long getRoutingNumber() {
		return routingNumber;
	}

	public void setRoutingNumber(long routingNumber) {
		this.routingNumber = routingNumber;
	}

	public long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public double getInterest() {
		return interest;
	}

	public void setInterest(double interest) {
		this.interest = interest;
	}

	private String type;
	private long routingNumber;
	private long accountNumber;
	private double balance;
	private double interest;

	public Account() {
		balance = 0;
		interest = 0;
	}

	public Account(long accountNumber, long routingNumber, String type, double initialBalance, double initialInterest) {
		this.accountNumber = accountNumber;
		this.routingNumber = routingNumber;
		this.type = type;
		this.balance = initialBalance;
		this.interest = initialInterest;
	}
	public String getType() {
		return type;
	}

	public void deposit(double amount) {
		balance = balance + amount;
	}

	public void withdraw(double amount) {
		balance = balance - amount;
	}

	public void addInterest() {
		balance = balance + balance * interest;
	}

	public String toString() {
		return "" + this.accountNumber + ", " + this.routingNumber;
	}
}
